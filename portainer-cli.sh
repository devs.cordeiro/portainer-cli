#!/bin/bash

####
# Arguments : 
# 1. Stack action (c: create, r: read, u: update, d: delete, l:list)
# 2. Endpoint identifier
# 3. Swarm identifier
# 4. Stack name
# 5. Stack filepath
####
## Exemple : l
STACK_ACTION=${1:-unset}
## Exemple : 1
ENDPOINT_ID=${2:-unset}
## Exemple : 3d9zi8a879tdf3jrmllvepqkl
SWARM_ID=${3:-unset}
## Exemple : ortolangtest
STACK_NAME=${4:-unset}
## Example : ./docker-stack.yml
STACK_FILEPATH=${5:-unset}

## Environment variables
DEPLOY_PORTAINER_HOST=${PORTAINER_HOST:-unset}
DEPLOY_PORTAINER_USERNAME=${PORTAINER_USERNAME:-unset}
DEPLOY_PORTAINER_PASSWORD=${PORTAINER_PASSWORD:-unset}
DEPLOY_PORTAINER_JWT=${PORTAINER_JWT:-unset}

function check_env {
    if [ $1 == 'unset' ]; then
        echo "$2" 1>&2
        exit 1
    fi
}

function create_stack {
    echo "Creating stack ..."
    http --ignore-stdin --check-status --follow -f -b POST "${PORTAINER_HOST}/api/stacks?type=1&method=file&endpointId=${ENDPOINT_ID}" "Authorization: Bearer $JWT" "Name=$STACK_NAME" "EndpointID=$ENDPOINT_ID" "SwarmID=$SWARM_ID" "file@${STACK_FILEPATH}"
    STATUS_RETURNED=$?
    return $STATUS_RETURNED
}
function update_stack {
    search_stack
    if [ -z "$STACK_FOUND" ]; then 
        echo "Error : Stack not found"
        STATUS_RETURNED=1
    else
        echo "Updating stack $STACK_ID_FOUND ..."
        file_content=$(<$STACK_FILEPATH)
        http --ignore-stdin --check-status -j --follow PUT "${PORTAINER_HOST}/api/stacks/${STACK_ID_FOUND}?endpointId=${ENDPOINT_ID}" "Authorization: Bearer $JWT" "id=${STACK_ID_FOUND}" "Env:=[]" "Prune:=false" "StackFileContent=$file_content"
        STATUS_RETURNED=$?
        echo "Updated !!"
    fi
    return $STATUS_RETURNED
}
function delete_stack {
    search_stack
    if [ -z "$STACK_FOUND" ]; then 
        echo "Error : Stack not found"
        STATUS_RETURNED=1
    else
        echo "Deleting stack $STACK_ID_FOUND ..."
        http --ignore-stdin --check-status -v DELETE "${PORTAINER_HOST}/api/stacks/${STACK_ID_FOUND}" "Authorization: Bearer $JWT"
        STATUS_RETURNED=$?
        echo "Deleted !!"
    fi
    return $STATUS_RETURNED
}
function list_stack {
    http --ignore-stdin --check-status --follow -b GET "${PORTAINER_HOST}/api/stacks" "Authorization: Bearer $JWT" > STACKS
    STATUS_RETURNED=$?
    STACKS=$(cat STACKS)
    rm -f STACKS
    return $STATUS_RETURNED
}
function search_stack {
    list_stack
    echo $STACKS > STACKS_DEPLOYED
    STACK_FOUND=$(cat STACKS_DEPLOYED | jq ".[] | select(.Name == \"$STACK_NAME\")")
    if [ ! -z "$STACK_FOUND" ]; then 
        echo "Stack found : $STACK_FOUND"
        STACK_ID_FOUND=$(cat STACKS_DEPLOYED | jq ".[] | select(.Name == \"$STACK_NAME\") | .Id")
        echo "Stack id found : $STACK_ID_FOUND"
        STATUS_RETURNED=0
    else
        STATUS_RETURNED=1
    fi
    rm -f STACKS_DEPLOYED
    return $STATUS_RETURNED
}
function deploy_stack {
    search_stack
    if [ -z "$STACK_FOUND" ]; then 
        echo "Creating stack"
        create_stack
        STATUS_RETURNED=$?
    else
        update_stack 
        # delete_stack
        # echo "Waiting 10s ..."
        # sleep 10s
        # create_stack
        STATUS_RETURNED=$?
    fi
    return $STATUS_RETURNED
}

check_env $DEPLOY_PORTAINER_HOST "Sets PORTAINER_HOST environment variable."
check_env $DEPLOY_PORTAINER_USERNAME "Sets PORTAINER_USERNAME environment variable."
check_env $DEPLOY_PORTAINER_PASSWORD "Sets PORTAINER_PASSWORD environment variable."
check_env $STACK_ACTION "Mission first argument : stack acctin."

if [ $DEPLOY_PORTAINER_JWT == 'unset' ]; then
    http --ignore-stdin --pretty=none -b POST ${PORTAINER_HOST}/api/auth Username=$PORTAINER_USERNAME Password=$PORTAINER_PASSWORD > LOGIN
    JWT=$(cat LOGIN | jq -r '.jwt')
    echo "Please sets PORTAINER_JWT by this '$JWT' for the next time"
    echo "------------------------------------------------------"
## Uses variable instead of file LOGIN
# LOGIN=$(http --ignore-stdin --pretty=none -b POST '$PORTAINER_HOST/api/auth' Username=$PORTAINER_USERNAME Password=$PORTAINER_PASSWORD)
# JWT=$(echo $LOGIN | jq -r '.jwt')
else
    JWT=$DEPLOY_PORTAINER_JWT
fi

STATUS=1

if [ $STACK_ACTION == 'l' ]; then
    list_stack
    STATUS=$?
    echo "Listes des stacks : $STACKS"
fi
if [ $STACK_ACTION == 's' ]; then
    check_env $ENDPOINT_ID "Mission second argument : endpoint identifier."
    check_env $SWARM_ID "Mission third argument : swarm identifier."
    check_env $STACK_NAME "Mission forth argument : stack name."
    search_stack
    STATUS=$?
fi
if [ $STACK_ACTION == 'c' ]; then
    check_env $ENDPOINT_ID "Mission second argument : endpoint identifier."
    check_env $SWARM_ID "Mission third argument : swarm identifier."
    check_env $STACK_NAME "Mission forth argument : stack name."
    check_env $STACK_FILEPATH "Mission fifth argument : stack file path."
    create_stack
    STATUS=$?
fi
if [ $STACK_ACTION == 'd' ]; then
    check_env $ENDPOINT_ID "Mission second argument : endpoint identifier."
    check_env $SWARM_ID "Mission third argument : swarm identifier."
    check_env $STACK_NAME "Mission forth argument : stack name."
    delete_stack 
    STATUS=$?
fi
if [ $STACK_ACTION == 'u' ]; then
    check_env $ENDPOINT_ID "Mission second argument : endpoint identifier."
    check_env $SWARM_ID "Mission third argument : swarm identifier."
    check_env $STACK_NAME "Mission forth argument : stack name."
    check_env $STACK_FILEPATH "Mission fifth argument : stack file path."
    update_stack 
    STATUS=$?
fi
if [ $STACK_ACTION == 'dp' ]; then
    check_env $ENDPOINT_ID "Mission second argument : endpoint identifier."
    check_env $SWARM_ID "Mission third argument : swarm identifier."
    check_env $STACK_NAME "Mission forth argument : stack name."
    check_env $STACK_FILEPATH "Mission fifth argument : stack file path."
    deploy_stack 
    STATUS=$?
fi

rm -f LOGIN
exit $STATUS