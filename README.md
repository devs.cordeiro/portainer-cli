# Portainer comand line

Script de déploiement dans le service Portainer.

## Docker
L'image Docker permet le déploiement dans Portainer via le script deploy.sh. 

Pour tester la compatibilité avec GitLab CI : 
```
docker build --rm -f "Dockerfile" -t portainer-cli:latest .
docker rm -f build
docker run --name build -i portainer-cli /bin/bash < gitlab/build_script
```
Voir https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#how-to-debug-a-job-locally

Pour tester le Dockerfile :
```
docker run --name build -i -e PORTAINER_HOST=portainer.localhost -e PORTAINER_USERNAME=admin -e PORTAINER_PASSWORD=tagada portainer-cli deploy endpoindId swarmId stackName swarmFilepath
```
