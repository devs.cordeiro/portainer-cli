FROM ubuntu

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y \
    httpie \
    jq \
    gettext-base \
    git
COPY ./portainer-cli.sh /opt/portainer-cli/
RUN ln -s /opt/portainer-cli/portainer-cli.sh /usr/local/bin/portainer-cli
# ENTRYPOINT [ "/bin/bash" ]
